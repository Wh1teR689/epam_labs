﻿namespace Reselling.Business.Models
{
    public class Ticket
    {
        public int Id { get; set; }
        public Event Event { get; set; }
        public decimal Price { get; set; }
        public User Seller { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }
    }
}
