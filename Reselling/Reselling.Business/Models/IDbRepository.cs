﻿using System.Collections.Generic;
using System.Linq;

namespace Reselling.Business.Models
{
    public interface IDbRepository<T>
    {
        IQueryable<T> GetAll();
        T GetById(int id);
        void Create(T item);
        void Update(T item);
        void Delete(int id);
    }
}
