﻿namespace Reselling.Business.Models
{
    public class Order
    {
        public int Id { get; set; }
        public Ticket Ticket { get; set; }
        public string Status { get; set; }
        public User Buyer { get; set; }
        public string TrackingNumber { get; set; }
        public string Comment { get; set; }
    }
}
