﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class VenuesRepository : IDbRepository<Venue>
    {
        ResellingDbContext _context;

        public VenuesRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(Venue item)
        {
            _context.Venues.Add(item);
        }

        public void Delete(int id)
        {
            Venue venue = _context.Venues.FirstOrDefault(v => v.Id == id);
            if (venue != null)
            {
                _context.Venues.Remove(venue);
            }
        }

        public IQueryable<Venue> GetAll()
        {
            return _context.Venues.Include(v => v.City);
        }

        public Venue GetById(int id)
        {
            return _context.Venues.Include(v => v.City).FirstOrDefault(v => v.Id == id);
        }

        public void Update(Venue item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
