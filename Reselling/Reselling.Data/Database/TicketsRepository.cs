﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class TicketsRepository : IDbRepository<Ticket>
    {
        ResellingDbContext _context;

        public TicketsRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(Ticket item)
        {
            _context.Tickets.Add(item);
        }

        public void Delete(int id)
        {
            Ticket ticket = _context.Tickets.FirstOrDefault(t => t.Id == id);
            if (ticket != null)
            {
                _context.Tickets.Remove(ticket);
            }
        }

        public IQueryable<Ticket> GetAll()
        {
            return _context.Tickets.Include(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Seller).ThenInclude(u => u.Role);
        }

        public Ticket GetById(int id)
        {
            return _context.Tickets.Include(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Seller).ThenInclude(u => u.Role).FirstOrDefault(t => t.Id == id);
        }

        public void Update(Ticket item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}