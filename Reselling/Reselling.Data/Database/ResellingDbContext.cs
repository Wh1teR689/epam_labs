﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Reselling.Data.Database
{
    public class ResellingDbContext : DbContext
    {
        public ResellingDbContext(DbContextOptions<ResellingDbContext> options) : base(options) { }

        public DbSet<City> Cities { get; set; }
        public DbSet<Venue> Venues { get; set; }
        public DbSet<Event> Events { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<Order> Orders { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Venue -> City
            modelBuilder.Entity<Venue>()
                .HasOne(v => v.City)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // Event -> Venue
            modelBuilder.Entity<Event>()
                .HasOne(e => e.Venue)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // User -> Role
            modelBuilder.Entity<User>()
                .HasOne(u => u.Role)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // Ticket -> Event
            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Event)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // Ticket -> User
            modelBuilder.Entity<Ticket>()
                .HasOne(t => t.Seller)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // Order -> Ticket
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Ticket)
                .WithMany()
                .OnDelete(DeleteBehavior.Cascade);

            // Order -> User
            modelBuilder.Entity<Order>()
                .HasOne(o => o.Buyer)
                .WithMany();
                
        }
}
}
