﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class UsersRepository : IDbRepository<User>
    {
        ResellingDbContext _context;

        public UsersRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(User item)
        {
            _context.Users.Add(item);
        }

        public void Delete(int id)
        {
            User user = _context.Users.FirstOrDefault(u => u.Id == id);
            if (user != null)
            {
                _context.Users.Remove(user);
            }
        }

        public IQueryable<User> GetAll()
        {
            return _context.Users.Include(u => u.Role);
        }

        public User GetById(int id)
        {
            return _context.Users.Include(u => u.Role).FirstOrDefault(u => u.Id == id);
        }

        public void Update(User item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
