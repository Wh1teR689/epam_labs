﻿using System;

namespace Reselling.Data.Database
{
    public class UnitOfWork
    {
        static ResellingDbContext _context;
        
        Lazy<CitiesRepository> _citiesRepository = new Lazy<CitiesRepository>(() => new CitiesRepository(_context), true);
        Lazy<EventsRepository> _eventsRepository = new Lazy<EventsRepository>(() => new EventsRepository(_context), true);
        Lazy<OrdersRepository> _ordersRepository = new Lazy<OrdersRepository>(() => new OrdersRepository(_context), true);
        Lazy<RolesRepository> _rolesRepository = new Lazy<RolesRepository>(() => new RolesRepository(_context), true);
        Lazy<TicketsRepository> _ticketsRepository = new Lazy<TicketsRepository>(() => new TicketsRepository(_context), true);
        Lazy<UsersRepository> _usersRepository = new Lazy<UsersRepository>(() => new UsersRepository(_context), true);
        Lazy<VenuesRepository> _venuesRepository = new Lazy<VenuesRepository>(() => new VenuesRepository(_context), true);
         
        public UnitOfWork(ResellingDbContext context)
        {
            _context = context;
        }

        public void Save()
        {
            _context.SaveChanges();
        }

        public CitiesRepository Cities
        {
            get
            {              
                return _citiesRepository.Value;
            }
        }


        public EventsRepository Events
        {
            get
            {
                return _eventsRepository.Value;
            }

        }

        public OrdersRepository Orders
        {
            get
            {
                return _ordersRepository.Value;
            }

        }

        public RolesRepository Roles
        {
            get
            {
                return _rolesRepository.Value;
            }
        }

        public TicketsRepository Tickets
        {
            get
            {
                return _ticketsRepository.Value;
            }

        }

        public UsersRepository Users
        {
            get
            {
                return _usersRepository.Value;
            }

        }

        public VenuesRepository Venues
        {
            get
            {
                return _venuesRepository.Value;
            }

        }
    }
}
