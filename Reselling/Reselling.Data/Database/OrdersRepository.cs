﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class OrdersRepository : IDbRepository<Order>
    {
        ResellingDbContext _context;

        public OrdersRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(Order item)
        {
            _context.Orders.Add(item);
        }

        public void Delete(int id)
        {
            Order order = _context.Orders.FirstOrDefault(o => o.Id == id);
            if (order != null)
            {
                _context.Orders.Remove(order);
            }
        }

        public IQueryable<Order> GetAll()
        {
            return _context.Orders.Include(o => o.Ticket).ThenInclude(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Buyer).ThenInclude(u => u.Role);
        }

        public Order GetById(int id)
        {
            return _context.Orders.Include(o => o.Ticket).ThenInclude(t => t.Event).ThenInclude(e => e.Venue).ThenInclude(v => v.City).Include(t => t.Buyer).ThenInclude(u => u.Role).FirstOrDefault(o => o.Id == id);
        }

        public void Update(Order item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
