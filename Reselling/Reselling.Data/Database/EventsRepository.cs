﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class EventsRepository : IDbRepository<Event>
    {
        ResellingDbContext _context;

        public EventsRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(Event item)
        {
            _context.Events.Add(item);
        }

        public void Delete(int id)
        {
            Event item = _context.Events.FirstOrDefault(e => e.Id == id);
            if (item != null)
            {
                _context.Events.Remove(item);
            } 
        }

        public IQueryable<Event> GetAll()
        {
            return _context.Events.Include(e => e.Venue).ThenInclude(v => v.City);
        }

        public Event GetById(int id)
        {
            return _context.Events.Include(e => e.Venue).ThenInclude(v => v.City).FirstOrDefault(e => e.Id == id);
        }

        public void Update(Event item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
