﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class RolesRepository : IDbRepository<Role>
    {
        ResellingDbContext _context;

        public RolesRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(Role item)
        {
            _context.Roles.Add(item);
        }

        public void Delete(int id)
        {
            Role role = _context.Roles.FirstOrDefault(r => r.Id == id);
            if (role != null)
            {
                _context.Roles.Remove(role);
            }
        }
        
        public IQueryable<Role> GetAll()
        {
            return _context.Roles;
        }

        public Role GetById(int id)
        {
            return _context.Roles.FirstOrDefault(r => r.Id == id);
        }

        public void Update(Role item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
