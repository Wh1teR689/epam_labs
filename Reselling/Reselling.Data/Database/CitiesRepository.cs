﻿using Microsoft.EntityFrameworkCore;
using Reselling.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Data.Database
{
    public class CitiesRepository : IDbRepository<City>
    {
        private ResellingDbContext _context;

        public CitiesRepository(ResellingDbContext context)
        {
            _context = context;
        }

        public void Create(City item)
        {
            _context.Cities.Add(item);
        }

        public void Delete(int id)
        {
            City city = _context.Cities.FirstOrDefault(c => c.Id == id);
            if (city != null)
            {
                _context.Cities.Remove(city);
            }
        }

        public IQueryable<City> GetAll()
        {
            return _context.Cities;
        }

        public City GetById(int id)
        {
            return _context.Cities.FirstOrDefault(c => c.Id == id);
        }

        public void Update(City item)
        {
            _context.Entry(item).State = EntityState.Modified;
        }
    }
}
