﻿using Microsoft.AspNetCore.Builder;
using Reselling.Data.Database;
using Microsoft.Extensions.DependencyInjection;
using System.Linq;
using Reselling.Business.Models;
using System;

namespace Reselling.Presentation
{
    public static class DataSeeder
    {
        // TODO: Move this code when seed data is implemented in EF 7

        /// <summary>
        /// This is a workaround for missing seed data functionality in EF 7.0-rc1
        /// More info: https://github.com/aspnet/EntityFramework/issues/629
        /// </summary>
        /// <param name="app">
        /// An instance that provides the mechanisms to get instance of the database context.
        /// </param>
        public static void SeedData(this IApplicationBuilder app)
        {
            var db = app.ApplicationServices.GetService<ResellingDbContext>();

            if (!db.Roles.Any())
            {

                #region Cities initialization
                db.Cities.AddRange(
                    new City() { Name = "Гомель" },
                    new City() { Name = "Минск" },
                    new City() { Name = "Москва" },
                    new City() { Name = "Питер" },
                    new City() { Name = "Брест" },
                    new City() { Name = "Екатеринбург" }
                );
                db.SaveChanges();
                #endregion

                #region Venues initialization
                db.Venues.AddRange(
                    new Venue() { Name = "Ледовый дворец", City = db.Cities.FirstOrDefault(c => c.Id == 1), Address = "ул. Мазурова, 110" },
                    new Venue() { Name = "Гомельский ГЦК", City = db.Cities.FirstOrDefault(c => c.Id == 1), Address = "ул. Ирининская, 16" },
                    new Venue() { Name = "Дворец Республики", City = db.Cities.FirstOrDefault(c => c.Id == 2), Address = "пл. Октябрьская, 1" },
                    new Venue() { Name = "TNT ROCK CLUB", City = db.Cities.FirstOrDefault(c => c.Id == 2), Address = "ул. Революционная, 9" },
                    new Venue() { Name = "Концертный зал 'Stadium Live'", City = db.Cities.FirstOrDefault(c => c.Id == 3), Address = "пр-кт Ленинградский, 80" },
                    new Venue() { Name = "Концертный зал 'Yotaspace'", City = db.Cities.FirstOrDefault(c => c.Id == 3), Address = "ул. Орджоникидзе, 11" },
                    new Venue() { Name = "Спортивно-концертный комплекс", City = db.Cities.FirstOrDefault(c => c.Id == 4), Address = "пр. Юрия Гагарина, 8" },
                    new Venue() { Name = "Клуб 'А2 Green Concert'", City = db.Cities.FirstOrDefault(c => c.Id == 4), Address = "пр. Медиков, 3" },
                    new Venue() { Name = "Кафе 'Авеню'", City = db.Cities.FirstOrDefault(c => c.Id == 5), Address = "ул. Советская, 15" },
                    new Venue() { Name = "Дк профсоюзов", City = db.Cities.FirstOrDefault(c => c.Id == 5), Address = "ул. Московская, 275" },
                    new Venue() { Name = "Бар 'New Bar'", City = db.Cities.FirstOrDefault(c => c.Id == 6), Address = "ул. 8 марта, 8" },
                    new Venue() { Name = "Дворец молодежи", City = db.Cities.FirstOrDefault(c => c.Id == 6), Address = "пр. Ленина, 1" }
                );
                db.SaveChanges();
                #endregion

                #region Events initialization
                db.Events.AddRange(
                    new Event()
                    {
                        Name = "Dark Tranquillity",
                        Date = new DateTime(2017, 01, 29),
                        Banner = "/images/events/DarkTranquillity.jpg",
                        Description = "Dark Tranquillity (англ. «Тёмное спокойствие», «Мрачное спокойствие») — шведская метал-группа, играющая в стиле мелодичный дэт-метал. Группа существует дольше всех групп из знаменитой метал-сцены Гётеборга, а также претендует на участие в создании особенного гётеборгского звучания.",
                        Venue = db.Venues.FirstOrDefault(v => v.Id == 6)
                    },
                    new Event()
                    {
                        Name = "Маша и Медведи. 20 лет",
                        Date = new DateTime(2017, 02, 14),
                        Banner = "/images/events/MashaIMedvedi.jpg",
                        Description = "Дюжину лет назад «Маша и Медведи» воссоединились, выпустив третью пластинку «Без языка», за которой последовали несколько синглов и интенсивные гастроли, доказавшие, что одним из самых ярких дебютантов 1990-х по-прежнему есть что сказать, а их солистке — что спеть. Между тем, с момента дебюта группы прошло вот уже 20 лет — чему и посвящено шоу в YOTASPACE.",
                        Venue = db.Venues.FirstOrDefault(v => v.Id == 6)
                    },
                    new Event()
                    {
                        Name = "Asking Alexandria",
                        Date = new DateTime(2017, 04, 15),
                        Banner = "/images/events/AskingAlexandria.jpg",
                        Description = "Asking Alexandria — самая узнаваемая молодая группа, сочетающая в своем творчестве рок-н-ролльный дух 90-х и актуальное звучание нового тысячелетия. За сравнительно короткое для рок-коллективов время британский пост-хардкор-проект прошел путь от полной неизвестности до мирового признания. В 2016 году Asking Alexandria признаны лучшей британской группой современности по версии ежегодной премии «Kerrang»!",
                        Venue = db.Venues.FirstOrDefault(v => v.Id == 4)
                    },
                    new Event()
                    {
                        Name = "Archive",
                        Date = new DateTime(2017, 06, 11),
                        Banner = "/images/events/Archive.jpg",
                        Description = "Лондонская группа Archive — отличная иллюстрация истины «нет пророка в своем отечестве»: оставаясь малоизвестными у себя на родине, в континентальной Европе (и особенно в соседней Франции) они давно приняты в элиту — выступая в амплуа фестивальных хедлайнеров и сотрудничая со звездами калибра Милен Фармер.",
                        Venue = db.Venues.FirstOrDefault(v => v.Id == 8)
                    },
                    new Event()
                    {
                        Name = "Oomph!",
                        Date = new DateTime(2017, 03, 26),
                        Banner = "/images/events/oomhp.jpg",
                        Description = "Oomph! — немецкая рок-группа, исполняющая музыку в стиле Neue Deutsche Härte. Группа была образована в 1989 году в Вольфсбурге, ФРГ.",
                        Venue = db.Venues.FirstOrDefault(v => v.Id == 11)
                    }
                );
                db.SaveChanges();
                #endregion

                #region Roles initialization
                db.Roles.AddRange(
                    new Role() { Name = "Admin" },
                    new Role() { Name = "User" }
                );
                db.SaveChanges();
                #endregion

                #region Users initialization
                db.Users.AddRange(
                    new User()
                    {
                        FirstName = "Alexander",
                        LastName = "Saponenko",
                        Localization = "ru",
                        Address = "г.Гомель, ул. Олимпийская 22, 5",
                        PhoneNumber = "+375441234567",
                        Login = "Admin",
                        Password = "Admin",
                        Role = db.Roles.FirstOrDefault(r => r.Id == 1)
                    },
                    new User()
                    {
                        FirstName = "Petr",
                        LastName = "Petrov",
                        Localization = "be",
                        Address = "г.Гомель, ул.Советская 12, 25",
                        PhoneNumber = "+375297654321",
                        Login = "User",
                        Password = "User",
                        Role = db.Roles.FirstOrDefault(r => r.Id == 2)
                    }
                );
                db.SaveChanges();
                #endregion

                #region Tickets initialization
                db.Tickets.AddRange(
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 1), Price = 20, Seller = db.Users.FirstOrDefault(u => u.Id == 1), Status = "free", Description = "Танцпол" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 1), Price = 15, Seller = db.Users.FirstOrDefault(u => u.Id == 1), Status = "free", Description = "Общий" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 2), Price = 12, Seller = db.Users.FirstOrDefault(u => u.Id == 1), Status = "free", Description = "Vip" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 2), Price = 32, Seller = db.Users.FirstOrDefault(u => u.Id == 2), Status = "free", Description = "Танцпол" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 3), Price = 13, Seller = db.Users.FirstOrDefault(u => u.Id == 2), Status = "free", Description = "Танцпол" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 3), Price = 24, Seller = db.Users.FirstOrDefault(u => u.Id == 1), Status = "sold", Description = "Vip" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 4), Price = 14, Seller = db.Users.FirstOrDefault(u => u.Id == 1), Status = "free", Description = "Танцпол" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 4), Price = 32, Seller = db.Users.FirstOrDefault(u => u.Id == 2), Status = "hidden", Description = "Общий" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 5), Price = 14, Seller = db.Users.FirstOrDefault(u => u.Id == 2), Status = "sold", Description = "Танцпол" },
                    new Ticket() { Event = db.Events.FirstOrDefault(e => e.Id == 5), Price = 17, Seller = db.Users.FirstOrDefault(u => u.Id == 2), Status = "free", Description = "Танцпол" }
                );
                db.SaveChanges();
                #endregion

                #region Orders initialization
                db.Orders.AddRange(
                    new Order() { Status = "Confirmed", Buyer = db.Users.FirstOrDefault(u => u.Id == 2), Ticket = db.Tickets.FirstOrDefault(t => t.Id == 6), TrackingNumber = "RU176536789" },
                    new Order() { Status = "Rejected", Buyer = db.Users.FirstOrDefault(u => u.Id == 2), Ticket = db.Tickets.FirstOrDefault(t => t.Id == 7), Comment = @"¯\_(ツ)_ /¯" },
                    new Order() { Status = "Waiting confirmation", Buyer = db.Users.FirstOrDefault(u => u.Id == 1), Ticket = db.Tickets.FirstOrDefault(t => t.Id == 8) },
                    new Order() { Status = "Confirmed", Buyer = db.Users.FirstOrDefault(u => u.Id == 1), Ticket = db.Tickets.FirstOrDefault(t => t.Id == 9), TrackingNumber = "RU123778339" }
                );
                db.SaveChanges();
                #endregion
            }
        }
    }
}
