﻿using Microsoft.AspNetCore.Localization;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Globalization;
using System.Text.RegularExpressions;

namespace Reselling.Presentation
{
    public class CustomRequestCultureProvider : IRequestCultureProvider
    {
        private CultureInfo defaultCulture;
        private CultureInfo defaultUICulture;

        public CustomRequestCultureProvider(RequestCulture requestCulture)
        {
            this.defaultCulture = requestCulture.Culture;
            this.defaultUICulture = requestCulture.UICulture;
        }

        public Task<ProviderCultureResult> DetermineProviderCultureResult(HttpContext httpContext)
        {
            PathString url = httpContext.Request.Path;
            
            if (url.ToString().Length <= 1)
            {
                // Set default
                return Task.FromResult<ProviderCultureResult>(new ProviderCultureResult(this.defaultCulture.TwoLetterISOLanguageName, this.defaultUICulture.TwoLetterISOLanguageName));
            }
            string currentCulture = httpContext.Request.Path.Value.Split('/').GetValue(1).ToString();

            if (!Regex.IsMatch(currentCulture, @"^[a-z]{2}(-[A-Z]{2})*$"))
            {
                // Set default
                return Task.FromResult<ProviderCultureResult>(new ProviderCultureResult(this.defaultCulture.TwoLetterISOLanguageName, this.defaultUICulture.TwoLetterISOLanguageName));
            }

            // Set current
            return Task.FromResult<ProviderCultureResult>(new ProviderCultureResult(currentCulture, currentCulture));
        }
    }
}
