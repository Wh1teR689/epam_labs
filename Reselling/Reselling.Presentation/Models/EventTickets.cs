﻿using Reselling.Business.Models;
using System.Collections.Generic;
using System.Linq;

namespace Reselling.Presentation.Models
{
    public class EventTickets
    {
        public Event Event;
        public List<Ticket> Tickets;

        public EventTickets(Event item, IEnumerable<Ticket> tickets)
        {
            Event = item;
            Tickets = tickets.ToList();
        }
    }
}
