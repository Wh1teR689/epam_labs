﻿using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Presentation.Models
{
    public class TicketModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The Price field is required.")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "The Description field is required.")]
        public string Description { get; set; }

        public int EventId { get; set; }
    }
}
