﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Presentation.Models
{
    public class EventModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The Name field is required.")]
        public string Name { get; set; }

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "The Date field is required.")]
        public DateTime Date { get; set; }

        [Required(ErrorMessage = "The Venue field is required.")]
        public int VenueId { get; set; }

        [Required(ErrorMessage = "The Description field is required.")]
        public string Description { get; set; }
    }
}
