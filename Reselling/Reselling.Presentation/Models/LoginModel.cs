﻿using System.ComponentModel.DataAnnotations;

namespace Reselling.Presentation.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "The Login field is required.")]
        public string Login { get; set; }

        [Required(ErrorMessage = "The Password field is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}
