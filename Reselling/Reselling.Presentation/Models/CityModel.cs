﻿using System.ComponentModel.DataAnnotations;

namespace Reselling.Presentation.Models
{
    public class CityModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "The city field is required.")]
        public string Name { get; set; }
    }
}

