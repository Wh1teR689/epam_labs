﻿using System.Collections.Generic;
using System.Linq;
using Reselling.Business.Models;

namespace Reselling.Presentation.Models
{
    public class UserTickets
    {
        public List<Ticket> SellingTickets { get; set; }
        public List<Order> WaitingConfirmationTickets { get; set; }
        public List<Order> SoldTickets { get; set; }

        public UserTickets(User user, List<Ticket> tickets, List<Order> orders)
        {
            SellingTickets = tickets.Where(item => item.Seller.Id == user.Id && item.Status == "free").ToList();
            WaitingConfirmationTickets = orders.Where(order => order.Ticket.Seller.Id == user.Id && order.Status == "Waiting confirmation").ToList();
            SoldTickets = orders.Where(order => order.Ticket.Seller.Id == user.Id && order.Ticket.Status == "sold" && order.Status == "Confirmed").ToList();
        }
    }
}
