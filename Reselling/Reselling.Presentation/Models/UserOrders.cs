﻿using Reselling.Business.Models;
using System.Collections.Generic;
using System.Linq;

namespace Reselling.Presentation.Models
{
    public class UserOrders
    {
        public List<Order> OrderedTickets { get; set; }

        public UserOrders(User user, List<Order> orders)
        {
            OrderedTickets = orders.Where(item => item.Buyer.Id == user.Id).ToList();
        }
    }
}
