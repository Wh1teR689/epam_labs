﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Presentation.Models.Confirmation
{
    public class RejectModel
    {
        [Required(ErrorMessage = "The Comment field is required.")]
        public string Comment { get; set; }

        public int OrderId { get; set; }
    }
}
