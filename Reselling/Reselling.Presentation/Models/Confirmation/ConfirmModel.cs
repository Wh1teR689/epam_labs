﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Presentation.Models.Confirmation
{
    public class ConfirmModel
    {
        [Required(ErrorMessage = "The Tracking Number field is required.")]
        public string TrackingNumber { get; set; }

        public int OrderId { get; set; }
    }
}
