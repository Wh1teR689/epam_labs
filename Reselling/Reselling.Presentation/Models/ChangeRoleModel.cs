﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Reselling.Presentation.Models
{
    public class ChangeRoleModel
    {
        public int UserId { get; set; }
        public string Login { get; set; }
        public int RoleId { get; set; }
    }
}
