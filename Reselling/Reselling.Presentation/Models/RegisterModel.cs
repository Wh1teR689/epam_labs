﻿using System.ComponentModel.DataAnnotations;

namespace Reselling.Presentation.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "The Login field is required.")]
        [Display(Name = "Login")]
        public string Login { get; set; }

        [Required(ErrorMessage = "The Password field is required.")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string PasswordСonfirmation { get; set; }
    }
}
