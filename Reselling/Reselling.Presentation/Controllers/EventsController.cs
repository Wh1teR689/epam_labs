﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Reselling.Data.Database;
using Microsoft.Extensions.Localization;
using Reselling.Presentation.Models;
using Reselling.Business.Models;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace Reselling.Presentation.Controllers
{
    public class EventsController : Controller
    {
        UnitOfWork _repo;
        private readonly IStringLocalizer<EventsController> _localizer;
        private IHostingEnvironment _appEnvironment;

        public EventsController(UnitOfWork repo, IStringLocalizer<EventsController> localizer, IHostingEnvironment appEnvironment)
        {
            _repo = repo;
            _localizer = localizer;
            _appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_repo.Events.GetAll().ToList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            EventModel model = new EventModel();
            model.Date = DateTime.Today;
            ViewBag.ListOfVenues = _repo.Venues.GetAll().ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Create(EventModel model)
        {
            if (ModelState.IsValid)
            {
                Venue venue = _repo.Venues.GetById(model.VenueId);
                Event item = _repo.Events.GetAll().FirstOrDefault(e => e.Name == model.Name && e.Date == model.Date && e.Venue == venue);
                if (item == null)
                {
                    item = new Event { Name = model.Name, Date = model.Date, Description = model.Description, Venue = venue, Banner = "/images/events/default_event.png" };
                    _repo.Events.Create(item);
                    _repo.Save();

                    return RedirectToAction("Index", "Events");
                }
                else
                {
                    ModelState.AddModelError("", _localizer["The Event already exists"]);
                }
            }

            ViewBag.ListOfVenues = _repo.Venues.GetAll().ToList();
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            Event eventForUpdate = _repo.Events.GetById(id);
            EventModel model = new EventModel() {
                Id = eventForUpdate.Id,
                Date = eventForUpdate.Date,
                Description = eventForUpdate.Description,
                Name = eventForUpdate.Name,
                VenueId = eventForUpdate.Venue.Id
            };
            ViewBag.ListOfVenues = _repo.Venues.GetAll().ToList();
            return View(model);
        }

        [HttpPost]
        public IActionResult Update(EventModel model)
        {
            if (ModelState.IsValid)
            {
                Event item = _repo.Events.GetById(model.Id);
                if (item != null)
                {
                    item.Name = model.Name;
                    item.Date = model.Date;
                    item.Description = model.Description;
                    item.Venue = _repo.Venues.GetById(model.VenueId);
                    _repo.Events.Update(item);
                    _repo.Save();

                    return RedirectToAction("Index", "Events");
                }
                else
                {
                    ModelState.AddModelError("", _localizer[@"wrong Id"]);
                }
            }
            ViewBag.ListOfVenues = _repo.Venues.GetAll().ToList();
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _repo.Events.Delete(id);
            _repo.Save();
            return RedirectToAction("Index", "Events");
        }


        [HttpGet]
        public IActionResult LoadImage(int id)
        {
            ViewBag.EventId = id;
            return View(id);
        }

        [HttpPost]
        public async Task<IActionResult> LoadImage(IFormFile uploadedFile, int eventId)
        {
            if (uploadedFile != null)
            {
                Event item = _repo.Events.GetById(eventId);
                if (item != null)
                {
                    item.Banner = "/images/events/" + item.Id + "." + uploadedFile.FileName.Split('.').Last();
                    _repo.Events.Update(item);
                    _repo.Save();
                }

                using (var fileStream = new FileStream(_appEnvironment.WebRootPath + "/images/events/" + item.Id + "." + uploadedFile.FileName.Split('.').Last(), FileMode.Create))
                {
                    await uploadedFile.CopyToAsync(fileStream);
                }
            }
            return RedirectToAction("Index", "Events");
        }
    }
}
