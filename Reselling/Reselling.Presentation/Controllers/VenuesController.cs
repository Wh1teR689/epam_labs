﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Reselling.Data.Database;
using Microsoft.Extensions.Localization;
using Reselling.Presentation.Models;
using Reselling.Business.Models;

namespace Reselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class VenuesController : Controller
    {
        UnitOfWork _repo;
        private readonly IStringLocalizer<VenuesController> _localizer;

        public VenuesController(UnitOfWork repo, IStringLocalizer<VenuesController> localizer)
        {
            _repo = repo;
            _localizer = localizer;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View(_repo.Venues.GetAll().ToList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            IEnumerable<City> cityList = _repo.Cities.GetAll().ToList();
            ViewBag.ListOfCity = cityList;
            VenueModel venueModel = new VenueModel();
            return View(venueModel);
        }

        [HttpPost]
        public IActionResult Create(VenueModel model)
        {
            if (ModelState.IsValid) 
            {
                City city = _repo.Cities.GetById(model.CityId);
                Venue venue = _repo.Venues.GetAll().FirstOrDefault(v => v.Name == model.Name && v.City == city && v.Address == model.Address);
                if (venue == null)
                {
                    venue = new Venue() { Name = model.Name, City = city, Address = model.Address };
                    _repo.Venues.Create(venue);
                    _repo.Save();
                    return RedirectToAction("Index", "Venues");
                }
                else
                {
                    ModelState.AddModelError("", _localizer["The venue already exists"]);
                }
            }
            IEnumerable<City> cityList = _repo.Cities.GetAll();
            ViewBag.ListOfCity = cityList;
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            ViewBag.ListOfCity = _repo.Cities.GetAll().ToList();

            Venue venue = _repo.Venues.GetById(id);
            VenueModel venueModel = new VenueModel() { Name = venue.Name, Address = venue.Address, CityId =  venue.City.Id};
            return View(venueModel);
        }

        [HttpPost]
        public IActionResult Update(VenueModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _repo.Cities.GetById(model.CityId);
                Venue venue = _repo.Venues.GetById(model.Id);
                if (venue != null)
                {
                    venue.Name = model.Name;
                    venue.Address = model.Address;
                    venue.City = city;

                    _repo.Venues.Update(venue);
                    _repo.Save();
                    return RedirectToAction("Index", "Venues");
                }
                else
                {
                    ModelState.AddModelError("", _localizer[@"Something happened ¯\_(ツ)_ /¯, try later.."]);
                }
            }
            ViewBag.ListOfCity = _repo.Cities.GetAll().ToList();
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _repo.Venues.Delete(id);
            _repo.Save();
            return RedirectToAction("Index", "Venues");
        }
    }
}
