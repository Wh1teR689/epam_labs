﻿using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Reselling.Presentation.Models;
using Microsoft.AspNetCore.Authorization;
using Reselling.Business.Models;
using Reselling.Data.Database;

namespace Reselling.Presentation.Controllers
{
    public class UserController : Controller
    {
        UnitOfWork _repo;

        public UserController(UnitOfWork repo)
        {
            _repo = repo;
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult UserOrders()
        {
            User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            UserOrders userOrders = new UserOrders(user, _repo.Orders.GetAll().ToList());
            return View(userOrders);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult UserTickets()
        {
            User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            UserTickets userTickets = new UserTickets(user, _repo.Tickets.GetAll().ToList(), _repo.Orders.GetAll().ToList());
            return View(userTickets);
        }

        [HttpGet]
        [Authorize(Roles = "Admin, User")]
        public IActionResult UserInfo()
        {
            User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
            return View(user);
        }

        public IActionResult UserList()
        {
            return View(_repo.Users.GetAll().ToList());
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRole(int id)
        {
            User user = _repo.Users.GetById(id);
            ChangeRoleModel userForChange = new ChangeRoleModel() { UserId = user.Id, Login = user.Login, RoleId = user.Role.Id };
            ViewBag.RolesList = _repo.Roles.GetAll().ToList();
            return View(userForChange);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public IActionResult ChangeRole(ChangeRoleModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _repo.Users.GetById(model.UserId);
                if (user != null)
                {
                    user.Role = _repo.Roles.GetById(model.RoleId);
                    _repo.Users.Update(user);
                    _repo.Save();

                    return RedirectToAction("UserList");
                }
                else
                {
                    ModelState.AddModelError("", @"Something happened ¯\_(ツ)_ /¯, try later..");
                }
            }
            ViewBag.RolesList = _repo.Roles.GetAll().ToList();
            return View(model);
        }

        [HttpGet]
        [Authorize]
        public IActionResult ChangeOwnData(int id)
        {
            User user = _repo.Users.GetById(id);
            return View(user);
        }

        [HttpPost]
        [Authorize]
        public IActionResult ChangeOwnData(User model)
        {
            if (ModelState.IsValid)
            {
                User user = _repo.Users.GetById(model.Id);
                if (user != null)
                {
                    user.FirstName = model.FirstName;
                    user.LastName = model.LastName;
                    user.Address = model.Address;
                    user.Localization = model.Localization;
                    user.PhoneNumber = model.PhoneNumber;
                    _repo.Users.Update(user);
                    _repo.Save();
                    RouteData.Values["culture"] = model.Localization;
                    return RedirectToAction("UserInfo");
                }
                else
                {
                    ModelState.AddModelError("", @"Something happened ¯\_(ツ)_ /¯, try later..");
                }
            }
            return View(model);
        }

    }
}
