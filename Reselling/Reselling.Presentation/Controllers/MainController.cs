﻿using Microsoft.AspNetCore.Mvc;
using Reselling.Data.Database;
using System.Linq;

namespace Reselling.Presentation.Controllers
{
    public class MainController : Controller
    {
        UnitOfWork _repo;

        public MainController(UnitOfWork repo)
        {
            _repo = repo;
        }
        
        public IActionResult Events()
        {
            return View(_repo.Events.GetAll().Where(e => e.Date >= System.DateTime.Now.Subtract(new System.TimeSpan(1,0,0,0))).ToList());
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }
    }
}
