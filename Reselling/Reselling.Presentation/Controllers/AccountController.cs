﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Reselling.Presentation.Models;
using Reselling.Business.Models;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Localization;
using Reselling.Data.Database;

namespace Reselling.Presentation.Controllers
{
    public class AccountController : Controller
    {
        private readonly IStringLocalizer<AccountController> localizer;
        UnitOfWork _repo;

        public AccountController(IStringLocalizer<AccountController> localizer, UnitOfWork repo)
        {
            this.localizer = localizer;
            _repo = repo;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == model.Login & u.Password == model.Password);
                if (user != null)
                {
                    await Authenticate(user);
                    RouteData.Values["culture"] = user.Localization ?? "en";
                    return RedirectToAction("Events", "Main");
                }
                ModelState.AddModelError("", localizer["Incorrect login or password"]);
            }
            return View(model);
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == model.Login);
                if (user == null)
                {
                    Role userRole = _repo.Roles.GetAll().FirstOrDefault(r => r.Name == "User");
                    User newUser = new User() { Login = model.Login, Password = model.Password, Role = userRole };
                    _repo.Users.Create(newUser);
                    _repo.Save();

                    await Authenticate(_repo.Users.GetAll().FirstOrDefault(u => u.Login == model.Login));
                    return RedirectToAction("Events", "Main");
                }
                ModelState.AddModelError("", localizer["A user with this login already exists"]);
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> LogOff()
        {
            await HttpContext.Authentication.SignOutAsync("Cookies");
            RouteData.Values["culture"] = "en";
            return RedirectToAction("Events", "Main");
        }


        private async Task Authenticate(User user)
        {
            List<Claim> claims = new List<Claim>
            {
                 new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                 new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role?.Name),
                 new Claim("UserName", (user.FirstName != null && user.LastName != null) ? user.FirstName + " " + user.LastName : "Anonymous")
            };

            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.Authentication.SignInAsync("Cookies", new ClaimsPrincipal(id));
        }
    }
}
