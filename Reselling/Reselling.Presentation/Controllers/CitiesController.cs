﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Reselling.Business.Models;
using Reselling.Data.Database;
using Reselling.Presentation.Models;
using System.Linq;

namespace Reselling.Presentation.Controllers
{
    [Authorize(Roles = "Admin")]
    public class CitiesController : Controller
    {
        UnitOfWork _repo;
        private readonly IStringLocalizer<AccountController> _localizer;

        public CitiesController(UnitOfWork repo, IStringLocalizer<AccountController> localizer)
        {
            _repo = repo;
            _localizer = localizer;
        }

        public IActionResult Index()
        {
            return View(_repo.Cities.GetAll().ToList());
        }

        [HttpGet]
        public IActionResult Create()
        {
            CityModel model = new CityModel();
            return View(model);
       }


        [HttpPost]
        public IActionResult Create(CityModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _repo.Cities.GetAll().FirstOrDefault(c => c.Name == model.Name);
                if (city == null)
                {
                    city = new City { Name = model.Name };
                    _repo.Cities.Create(city);
                    _repo.Save();

                    return RedirectToAction("Index", "Cities"); 
                }
                else
                {
                    ModelState.AddModelError("", _localizer["The city already exists"]);
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Update(int id)
        {
            City cityToUpdate = _repo.Cities.GetById(id);
            CityModel cityModelToUpdate = new CityModel() { Id = cityToUpdate.Id, Name = cityToUpdate.Name };
            return View(cityModelToUpdate);
        }

        [HttpPost]
        public IActionResult Update(CityModel model)
        {
            if (ModelState.IsValid)
            {
                City city = _repo.Cities.GetById(model.Id);
                if (city != null)
                {
                    city.Name = model.Name;
                    _repo.Cities.Update(city);
                    _repo.Save();

                    return RedirectToAction("Index", "Cities");
                }
                else
                {
                    ModelState.AddModelError("", _localizer[@"Something happened ¯\_(ツ)_ /¯, try later.."]);
                }
            }
            return View(model);
        }

        public IActionResult Delete(int id)
        {
            _repo.Cities.Delete(id);
            _repo.Save();
            return RedirectToAction("Index", "Cities");
        }
    }
}
