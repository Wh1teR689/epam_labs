﻿using Microsoft.AspNetCore.Mvc;
using Reselling.Data.Database;
using Reselling.Business.Models;
using Reselling.Presentation.Models.Confirmation;

namespace Reselling.Presentation.Controllers
{
    public class ConfirmationController : Controller
    {
        UnitOfWork _repo;

        public ConfirmationController(UnitOfWork repo)
        {
            _repo = repo;
        }

        [HttpGet]
        public IActionResult Confirm(int id)
        {
            ConfirmModel model = new ConfirmModel() { OrderId = id };
            return View(model);
        }

        [HttpPost]
        public IActionResult Confirm(ConfirmModel model)
        {
            if (ModelState.IsValid)
            {
                Order order = _repo.Orders.GetById(model.OrderId);
                if (order != null)
                {
                    // Order confirmed
                    order.Status = "Confirmed";
                    order.TrackingNumber = model.TrackingNumber;
                    _repo.Orders.Update(order);
                    _repo.Save();

                    // Ticket sold
                    Ticket ticket = _repo.Tickets.GetById(order.Ticket.Id);
                    ticket.Status = "sold";
                    _repo.Tickets.Update(ticket);
                    _repo.Save();

                    return RedirectToAction("UserTickets", "User");
                }
            }
            return View(model);
        }

        [HttpGet]
        public IActionResult Reject(int id)
        {
            RejectModel model = new RejectModel() { OrderId = id };
            return View(model);
        }

        [HttpPost]
        public IActionResult Reject(RejectModel model)
        {
            if (ModelState.IsValid)
            {
                Order order = _repo.Orders.GetById(model.OrderId);
                if (order != null)
                {
                    // Order rejected
                    order.Status = "Rejected";
                    order.Comment = model.Comment;
                    _repo.Orders.Update(order);
                    _repo.Save();

                    // Ticket free
                    Ticket ticket = _repo.Tickets.GetById(order.Ticket.Id);
                    ticket.Status = "free";
                    _repo.Tickets.Update(ticket);
                    _repo.Save();

                    return RedirectToAction("UserTickets", "User");
                }
            }
            return View(model);
        }
    }
}
