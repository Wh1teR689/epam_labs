﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Reselling.Business.Models;
using Reselling.Presentation.Models;
using Reselling.Data.Database;
using System.Linq;
using Microsoft.Extensions.Localization;
using Microsoft.AspNetCore.Authorization;

namespace Reselling.Presentation.Controllers
{
    public class TicketsController : Controller
    {
        UnitOfWork _repo;
        private readonly IStringLocalizer<TicketsController> _localizer;

        public TicketsController(UnitOfWork repo, IStringLocalizer<TicketsController> localizer)
        {
            _repo = repo;
            _localizer = localizer;
        }

        public IActionResult Tickets(int id)
        {
            Event currentEvent = _repo.Events.GetById(id);
            IEnumerable<Ticket> eventTickets = _repo.Tickets.GetAll().Where(ticket => ticket.Event.Id == id && ticket.Status == "free");
            return View(new EventTickets(currentEvent, eventTickets));
        }

        [HttpGet]
        [Authorize]
        public IActionResult Sell(int id)
        {
            TicketModel model = new TicketModel() { EventId = id };
            return View(model);
        }

        [HttpPost]
        [Authorize]
        public IActionResult Sell(TicketModel model)
        {
            if (ModelState.IsValid)
            {
                Event item = _repo.Events.GetById(model.EventId);
                Ticket ticket = new Ticket() { Price = model.Price, Event = item, Status = "free", Description = model.Description, Seller = _repo.Users.GetAll().FirstOrDefault(u => u.Login == User.Identity.Name) };

                _repo.Tickets.Create(ticket);
                _repo.Save();

                return RedirectToAction("Tickets", new { id = model.EventId });
            }
            return View(model);
        }
        
        [Authorize]
        public IActionResult Buy(int id)
        {
            Ticket ticket = _repo.Tickets.GetById(id);
            if(ticket != null)
            {
                // Hide ticket
                ticket.Status = "hidden";
                _repo.Tickets.Update(ticket);
                _repo.Save();

                //Create order
                User user = _repo.Users.GetAll().FirstOrDefault(u => u.Login == HttpContext.User.Identity.Name);
                Order order = new Order() { Ticket = ticket, Buyer = user, Status = "Waiting confirmation" };
                _repo.Orders.Create(order);
                _repo.Save();
                
                return RedirectToAction("Tickets", new { id = ticket.Event.Id });
            }
            return RedirectToAction("Tickets", new { id = ticket.Event.Id });
        }
    }
}
